import os
import click
import pandas as pd
import album_parser


@click.command()
@click.argument("directory", type=str)
def json_parser(directory: str) -> pd.DataFrame:
    """
    Takes in a directory where all the json albums are
    stored and parses it into one dataframe
    Args: directory (str)
    Returns: pd.DataFrame
    """
    # form of returned pd.DataFrame
    df = pd.DataFrame(
        columns=[
            "song_name",
            "album_name",
            "lyrics",
            "artist_names",
            "release_date",
            "hot",
        ]
    )
    # goes through relative path
    for filename in os.listdir(directory):
        temp_df = pd.DataFrame(
            columns=[
                "song_name",
                "album_name",
                "lyrics",
                "artist_names",
                "release_date",
                "hot",
            ]
        )
        # relative path + filename
        f = os.path.join(directory, filename)
        # parsing data into dataframe
        temp_df = album_parser(f)
        # adding all content into one dataframe
        df = pd.concat([temp_df, df], ignore_index=True)
        # deleting temp dataframe
        del temp_df

    return df
