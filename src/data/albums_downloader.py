import click
import lyricsgenius
import os


@click.command()
@click.argument('names', nargs=-1, type=str)
@click.argument("access_token", type=str)
@click.argument("dir", type=str)
def albums_downloader(names: list, access_token: str, dir="../../data/raw") -> None:
    """
    Takes in list with artist nicknames and downloads it in local folder in json format
    Args: names (list), access_token for Genius API (str),
    dir where to make a new folder with artist name and put all the jsons there (str)
    Returns: None
    """
    # initialize Genius API instance
    genius = lyricsgenius.Genius(access_token)
    # go through artists
    for i in names:
        genius.verbose = False  # Turn off status messages
        genius.remove_section_headers = (
            True  # Remove section headers (e.g. [Chorus]) from lyrics when searching
        )
        genius.skip_non_songs = (
            False  # Include hits thought to be non-songs (e.g. track lists)
        )
        genius.excluded_terms = [
            "(Remix)",
            "(Live)",
            "Romanized",
            "EnglishTranslation",
        ]  # Exclude albums with these words in their title

        # list of alums of each artist
        albums = genius.search_albums(i)

        os.chdir("..")
        # make a directory

        os.chdir(dir)

        if not os.path.exists(f"{i}"):
            os.mkdir(f"{i}")

        os.chdir(f"{i}")

        # goes through all the albums
        for j in range(len(albums["sections"][0]["hits"])):
            # checking for keywords (if there is, then we do not take them)
            if "Romanized" in albums["sections"][0]["hits"][j]["result"]["name"]:
                continue
            elif "Remix" in albums["sections"][0]["hits"][j]["result"]["name"]:
                continue
            elif (
                "EnglishTranslation"
                in albums["sections"][0]["hits"][j]["result"]["name"]
            ):
                continue
            elif "Live" in albums["sections"][0]["hits"][j]["result"]["name"]:
                continue
            # rerun search for download
            data = genius.search_album(
                albums["sections"][0]["hits"][j]["result"]["name"], i
            )
            # saving json
            data.save_lyrics()

        os.chdir("..")
        os.chdir("..")
        os.chdir("..")
        os.chdir("src")
        os.chdir("data")
