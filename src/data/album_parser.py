import click
import numpy as np
import pandas as pd
import json


@click.command()
@click.argument("relative_path", type=str)
def album_parser(relative_path: str) -> pd.DataFrame:
    """
    Args: relative path to parsed album (str)
    Returns: pd.DataFrame
    """
    # initialize an empty DataFrame
    df = pd.DataFrame(
        columns=[
            "song_name",
            "album_name",
            "lyrics",
            "artist_names",
            "release_date",
            "hot",
        ]
    )
    # open file
    with open(relative_path, "r") as album_file:
        # load json into python dict
        album = json.load(album_file)
        # iterate through tracks
        for i in range(len(album["tracks"])):
            # make a row into DataFrame (try / except if date is absent)
            try:
                new_row = pd.DataFrame(
                    {
                        "song_name": album["tracks"][i]["song"]["title"],
                        "album_name": album["name"],
                        "lyrics": album["tracks"][i]["song"]["lyrics"],
                        "artist_names": album["tracks"][i]["song"]["artist_names"],
                        "release_date": str(
                            album["tracks"][i]["song"]["release_date_components"][
                                "year"
                            ]
                        )
                        + " "
                        + str(
                            album["tracks"][i]["song"]["release_date_components"][
                                "month"
                            ]
                        )
                        + " "
                        + str(
                            album["tracks"][i]["song"]["release_date_components"]["day"]
                        ),
                        "hot": album["tracks"][i]["song"]["stats"]["hot"],
                    },
                    index=[i],
                )
            except KeyError:
                new_row = pd.DataFrame(
                    {
                        "song_name": album["tracks"][i]["song"]["title"],
                        "album_name": album["name"],
                        "lyrics": album["tracks"][i]["song"]["lyrics"],
                        "artist_names": album["tracks"][i]["song"]["artist_names"],
                        "release_date": np.NaN,
                        "hot": album["tracks"][i]["song"]["stats"]["hot"],
                    },
                    index=[i],
                )
            # add a row in DataFrame
            df = pd.concat([new_row, df], ignore_index=True)
    return df