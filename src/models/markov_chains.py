import numpy as np
import pandas as pd
from typing import Dict, List


def markov_chains_generator(df: pd.DataFrame, tokenized_text_field: str, n_words: int):
    """
    Args: df (pd.DataFrame), tokenized_text_field (str)
    Returns: (str)
    """
    # flattens a list of lists in one list via generator
    ind_words = [item for sublist in df[tokenized_text_field] for item in sublist]

    def make_pairs(ind_words):
        # returns pairs of consequent words and starts over where it
        # returned the prev. pair
        for i in range(len(ind_words) - 1):
            yield (ind_words[i], ind_words[i + 1])

        # a list of pairs of words

    pair = make_pairs(ind_words)
    word_dict: Dict[str, List[str]] = {}

    # word_1 as key and other words as values (one key could go with many values)
    for word_1, word_2 in pair:
        if word_1 in word_dict.keys():
            word_dict[word_1].append(word_2)
        else:
            word_dict[word_1] = [word_2]

    # pick a random first word
    chain = [np.random.choice(ind_words)]

    for i in range(n_words):
        chain.append(np.random.choice(word_dict[chain[-1]]))

    generated_text = " ".join(chain)
    return generated_text
