import re
import nltk
import pandas as pd
from pymorphy2 import MorphAnalyzer


def standardize_text(df: pd.DataFrame, text_field: str) -> pd.DataFrame:
    """
    Standardize text, clears all the unfavorable symbols,
    lowercases, punctuation e.t.c.
    Deletes constructions from Genius (e.g. num of contributors etc.)
    Args:df (pd.DataFrame), text_field (str)
    Returns: pd.DataFrame
    """

    df[text_field] = df[text_field].astype("str")

    punct = "!\"'\\#$%&*+,-.:;<=>?@[]^_`{|}~„“«»†*—-‘’"

    df[text_field] = df[text_field].str.replace("you might also like", repl="")
    df[text_field] = df[text_field].str.replace("lyrics", repl="")

    for i in range(df[text_field].shape[0]):
        lyrics_start = df[text_field].iloc[i].find("lyrics\n\n") + 7
        df[text_field].iloc[i] = df[text_field].iloc[i][
                                 lyrics_start + 8: (len(df[text_field].iloc[i])) - 7
                                 ]

    df[text_field] = df[text_field].str.replace(pat=punct, repl="")
    df[text_field] = df[text_field].str.replace("you might also like", repl="")
    df[text_field] = df[text_field].str.replace("lyrics", repl="")
    df[text_field] = df[text_field].str.replace(pat="\n", repl=" ")
    df[text_field] = df[text_field].str.replace(r"@\S+", "")
    df[text_field] = df[text_field].str.replace(
        r"[^А-Яа-яA-Za-z0-9ё()!?@\'\`\"\_\n]", " "
    )
    df[text_field] = df[text_field].str.lower()

    return df


def tokenizator(df: pd.DataFrame, text_field: str) -> pd.DataFrame:
    """
    Tokenizes text field, makes a new column with a listed words
    Args: df (pd.DataFrame), text_field (str)
    Returns: pandas.DataFrame
    """
    df["tokenized_text"] = df[text_field].apply(nltk.ToktokTokenizer().tokenize)

    return df


def lemmatizator(df: pd.DataFrame, text_field: str) -> pd.DataFrame:
    """
    Lemmatizes words with a morphological mark
    Args: df (pd.DataFrame), text_field (str)
    Returns: pandas.DataFrame
    """

    m = MorphAnalyzer()

    def lemmatize_word(token, pymorphy=m):
        return pymorphy.parse(token)[0].normal_form

    def lemmatize_sentence(sentence):
        return [lemmatize_word(w) for w in sentence]

    df["lemmatized_sentence"] = df[text_field].apply(lemmatize_sentence)

    return df


def adlib_finder(
        df: pd.DataFrame, text_field: str, delete_them=False, keep_brackets=True
) -> pd.DataFrame:
    """
    This function searches for phrases and interjections between brackets,
    which are referred as adlibs. And this content will be put in another column.
    Args: df (pd.DataFrame), text_field (str)
    Returns: pd.DataFrame
    """
    # initialize empty pd.Series
    df["adlibs"] = pd.Series([None] * df[text_field].shape[0])

    if delete_them:
        df["new_lyrics"] = pd.Series([None] * df[text_field].shape[0])

    for i in range(df[text_field].shape[0]):
        # locate all in-brackets symbols
        if keep_brackets:
            res = re.findall(r"\(.*?\)", df[text_field].iloc[i])
        else:
            res = re.findall(r"\((.*?)\)", df[text_field].iloc[i])

        if delete_them:
            df["new_lyrics"].iloc[i] = re.sub(
                "[([].*?[)]]", "", df[text_field].iloc[i]
            )

        # cahnge None to adlib
        df["adlibs"].iloc[i] = res[1: len(res) - 1]

    return df


def delete_extra(
        df: pd.DataFrame,
        columns=[
            "song_name",
            "album_name",
            "artist_names",
            "hot",
            "lyrics",
            "release_date",
        ],
):
    """
    Reduces unused columns to some extent
    Args: df (pd.DataFrame), columns (list)
    Returns: pd.DataFrame
    """
    df.drop(columns=columns, axis=1, inplace=True)
